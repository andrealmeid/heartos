heart.img: 
	nasm -fbin -o heart.img heart.asm

floppy_heart.img: heart.img
	rm -f floppy_heart.img
	dd if=/dev/zero of=floppy_heart.img bs=1024 count=1440
	dd if=heart.img of=floppy_heart.img conv=notrunc

.PHONY: heart.img floppy_heart.img 
qemu_heart: heart.img floppy_heart.img
	qemu-system-x86_64 -fda floppy_heart.img
