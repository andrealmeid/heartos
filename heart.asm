; SPDX-License-Identifier: GPL-3.0-or-later
; Copyright 2019 Chris Fallin <cfallin@c1f.net>. 
; Copyright 2020 André Almeida <andrealmeid@riseup.net>

VGA_ADDR equ 0x0A0000
SCREEN_SIZE equ 64000

[ORG 0x7c00]

	xor ax, ax ; make it zero
	mov ds, ax

	; Load first half of heart
	mov ah, 0x02
	mov al, (endtext - seg2 + 511) / 512  	; number of sectors
	mov ch, 0                               ; cylinder (0)
	mov cl, 2                               ; sector (2)
	mov dh, 0                               ; head (0)
	mov bx, seg2
	int 0x13

	; change into VGA mode 0x13 (320x200, 256 colors).
	mov ax, 0x0013
	int 0x10
    	mov edi, VGA_ADDR

	; paint my heart
    	mov edi, VGA_ADDR
	xor ax, ax
	mov cx, 320 * 100
	mov bx, heart

paint_loop:
	mov al, [bx]
	mov [edi], al
	inc edi

	inc bx
	dec cx
	jnz paint_loop

	; load the second half
	mov ah, 0x02
	mov al, 63			; number of sectors
	mov ch, 1			; cylinder (1)
	mov cl, 11			; sector (11)
	mov dh, 1			; head (1)
	mov bx, seg2
	int 0x13

	; paint more heart
	xor ax, ax
	mov cx, 320 * 100
	mov bx, heart

paint_loop2:
	mov al, [bx]
	mov [edi], al
	inc edi

	inc bx
	dec cx
	jnz paint_loop2

hang:
	jmp hang
	
times 510-($-$$) db 0
db 0x55
db 0xAA

; memory sectors with the heart
seg2:
%include "heart_array.asm"

times 512 * 64 - ($-$$) db 0
endtext:

seg3:
%include "heart_array2.asm"

endtext2:

